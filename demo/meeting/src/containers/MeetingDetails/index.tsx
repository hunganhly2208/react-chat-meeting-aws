// Copyright 2020-2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

import React from 'react';

import {
  Flex,
  Heading,
  PrimaryButton,
  useMeetingManager
} from 'amazon-chime-sdk-component-library-react';
import API, { graphqlOperation } from "@aws-amplify/api";
import { useHistory } from 'react-router-dom';
import { useAppState } from '../../providers/AppStateProvider';
import { StyledList } from './Styled';
import { createMessage } from '../../graphql/mutations';

const MeetingDetails = () => {
  const { meetingId, toggleTheme, theme } = useAppState();
  const manager = useMeetingManager();
  const roomId = sessionStorage.getItem("username")
  const history = useHistory();

  const goChatRoom = async () => {
    if (!roomId) {
      alert("No empty roomId");
      return;
    }

    const attendeeName = sessionStorage.getItem('attendeeName');
    const channelId = roomId+'_'+attendeeName;

    const input = {
      channelID: channelId,
      author: attendeeName,
      body: `*${attendeeName} entered the room!`,
    };

    try {
      await API.graphql(graphqlOperation(createMessage, { input }));
    } catch (error) {
      console.warn(error);
    }

    const notify = `${attendeeName} created a room chat #id #CID#${channelId}#CID#`;
    console.log('notify', notify)
    const inputForDefaultRoom = {
      channelID: roomId,
      author: attendeeName,
      body: notify,
    };

    try {
      await API.graphql(graphqlOperation(createMessage, { input: inputForDefaultRoom }));
    } catch (error) {
      console.warn(error);
    }

    history.push(`/chat-room?room=${channelId}&name=${attendeeName}`);
  };

  return (
    <Flex container layout="fill-space-centered">
      <Flex mb="2rem" mr={{ md: '2rem' }} px="1rem">
        <Heading level={4} tag="h1" mb={2}>
          Meeting information
        </Heading>
        <button onClick={goChatRoom}>Call to this host</button>
        <StyledList>
          <div>
            <dt>Meeting ID</dt>
            <dd>{meetingId}</dd>
          </div>
          <div>
            <dt>Hosted region</dt>
            <dd>{manager.meetingRegion}</dd>
          </div>
        </StyledList>
        <PrimaryButton
          mt={4}
          label={theme === 'light' ? 'Dark mode' : 'Light mode'}
          onClick={toggleTheme}
        ></PrimaryButton>
      </Flex>
    </Flex>
  );
};

export default MeetingDetails;
