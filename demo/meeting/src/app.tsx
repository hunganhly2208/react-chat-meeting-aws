// Copyright 2020-2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

import React, { FC } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import {
  lightTheme,
  MeetingProvider,
  NotificationProvider,
  darkTheme,
  GlobalStyles,
} from 'amazon-chime-sdk-component-library-react';

import { AppStateProvider, useAppState } from './providers/AppStateProvider';
import ErrorProvider from './providers/ErrorProvider';
import routes from './constants/routes';
import { NavigationProvider } from './providers/NavigationProvider';
import { Meeting, DeviceSetup, Home } from './views';
import Notifications from './containers/Notifications';
import NoMeetingRedirect from './containers/NoMeetingRedirect';
import meetingConfig from './meetingConfig';
import Child from './child';
import Chat from './chatp2p';

const App: FC = () => (
  <Router>
    <AppStateProvider>
      <Theme>
        <NotificationProvider>
          <Notifications />
          <ErrorProvider>
            <MeetingProvider {...meetingConfig}>
              <NavigationProvider>
                <Switch>
                  <Route exact path={routes.HOME} component={Home} />
                  <Route path={routes.DEVICE}>
                    <NoMeetingRedirect>
                      <DeviceSetup />
                    </NoMeetingRedirect>
                  </Route>
                  <Route path={routes.MEETING}>
                    <NoMeetingRedirect>
                      <Meeting />
                    </NoMeetingRedirect>
                  </Route>
                  <Route path={`${routes.CHAT}`}>
                    <Chat />
                  </Route>
                </Switch>
              </NavigationProvider>
            </MeetingProvider>
          </ErrorProvider>
        </NotificationProvider>
      </Theme>
    </AppStateProvider>
  </Router>
);

const Theme: React.FC = ({ children }) => {
  const { theme } = useAppState();
  const usrname = sessionStorage.getItem('username');
  if(!usrname) {
    sessionStorage.setItem('username', 'user1');
    window.location.reload();
  }

  const setCurrentListerning = (username: string) => {
    sessionStorage.setItem('username', username);
    window.location.reload();
  }

  return (
    <ThemeProvider theme={theme === 'light' ? lightTheme : darkTheme}>
      <GlobalStyles />
      <Child roomId={usrname} usrname={usrname}></Child>
      <div>
      <span>Current user: {usrname}</span><br></br>
      <button onClick={()=>setCurrentListerning('user1')}>Press to set current user: user1</button><br></br>
      <button onClick={()=>setCurrentListerning('user2')}>Press to set current user: user2</button><br></br>
      <button onClick={()=>setCurrentListerning('user3')}>Press to set current user: user3</button><br></br>
      </div>
      
      {children}
    </ThemeProvider>
  );
};

export default App;
