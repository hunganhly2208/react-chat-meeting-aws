import React, { useEffect, useState } from "react";
import {
  useLocation,
} from "react-router-dom";
import API, { graphqlOperation } from "@aws-amplify/api";
import "@aws-amplify/pubsub";
import { createMessage } from "./graphql/mutations";
import {
  onCreateMessageFilterById,
} from "./graphql/subscriptions";
import { messagesByChannelId } from "./graphql/queries";

import "./App.css";

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function Chat() {
  let query = useQuery();
  let room = query.get("room");
  const name = query.get("name") || "Anonymous";

  const [messages, setMessages] = useState<any>();
  const [messageBody, setMessageBody] = useState("");

  // Placeholder function for handling changes to our chat bar
  const handleChange = (event: any) => {
    setMessageBody(event.target.value);
  };

  // Placeholder function for handling the form submission
  const handleSubmit = async (event: any) => {
    event.preventDefault();
    event.stopPropagation();

    const input = {
      channelID: room,
      author: name,
      body: messageBody.trim(),
    };

    try {
      setMessageBody("");
      await API.graphql(graphqlOperation(createMessage, { input }));
    } catch (error) {
      console.warn(error);
    }
  };

  useEffect(() => {
      (API.graphql(
        graphqlOperation(messagesByChannelId, {
          channelID: room,
          sortDirection: "ASC",
        })
      ) as any).then((response: any) => {
        const items = response?.data?.messagesByChannelID?.items;
  
        if (items) {
          setMessages(items);
        }
      });
  }, []);

  useEffect(() => {
    const subscription = (API.graphql({
        query: onCreateMessageFilterById,
        variables: {
          channelID: room,
        },
      }) as any).subscribe({
      next: (event: any) => {
        const msg = event.value.data.onCreateMessageFilterById;
        setMessages([...messages, msg]);
      },
      error: (error: any) => {
        console.log("error: ", error);
      },
    });

    return () => {
      subscription.unsubscribe();
    };
  }, [messages]);

  return (
    <div className="container">
      <div className="messages">
        <div className="messages-scroller">
          {messages?.length && messages.map((message: any) => (
            <div
              key={message.id}
              className={message.author === name ? "message me" : "message"}
            >
              {message.body}
            </div>
          ))}
        </div>
      </div>
      <div className="chat-bar">
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            name="messageBody"
            placeholder="Type your message here"
            onChange={handleChange}
            value={messageBody}
          />
        </form>
      </div>
    </div>
  );
}

export default Chat;
