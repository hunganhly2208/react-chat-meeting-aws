import React, { useEffect, useState } from 'react';
import API from '@aws-amplify/api';
import { onCreateMessageFilterById } from './graphql/subscriptions';
import './child.css';
import { Link } from 'react-router-dom';

const Child = ({ roomId, usrname }: any) => {
  const [notifyMsg, setnotifyMsg] = useState('');
  useEffect(() => {
    const subscription = (API.graphql({
      query: onCreateMessageFilterById,
      variables: {
        channelID: roomId
      }
    }) as any).subscribe({
      next: (event: any) => {
        setnotifyMsg(event.value.data.onCreateMessageFilterById.body);
        setTimeout(() => {
          // setnotifyMsg('')
        }, 10000);
      },
      error: (error: any) => {
        console.log('error: ', error);
      }
    });

    return () => {
      subscription.unsubscribe();
    };
  }, [usrname]);

  const renderNotify = () => {
    console.log('notifyMsg', notifyMsg);
    let foundCID = notifyMsg.split('#CID#');
    const clickHereStr = `?room=${foundCID[1]}&name=${usrname}`;
    console.log('clickHereStr', clickHereStr)
    return (
      <div>
        A client has call you on room {foundCID[1]}. Close on 10s. Click{' '}
        <Link
          to={{
            pathname: '/chat-room',
            search: clickHereStr
          }}
        >
          here
        </Link>{' '}
        to: go to chat room
      </div>
    );
  };

  return (
    <>{notifyMsg && <div className="notify-modal">{renderNotify()}</div>}</>
  );
};

export default Child;
