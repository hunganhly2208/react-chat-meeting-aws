/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateMessageFilterById = /* GraphQL */ `
  subscription OnCreateMessageFilterById($channelID: String!) {
    onCreateMessageFilterById(channelID: $channelID) {
      id
      channelID
      author
      body
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateMessageFilterById = /* GraphQL */ `
  subscription OnUpdateMessageFilterById($channelID: String!) {
    onUpdateMessageFilterById(channelID: $channelID) {
      id
      channelID
      author
      body
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteMessageFilterById = /* GraphQL */ `
  subscription OnDeleteMessageFilterById($channelID: String!) {
    onDeleteMessageFilterById(channelID: $channelID) {
      id
      channelID
      author
      body
      createdAt
      updatedAt
    }
  }
`;
export const onCreateMessage = /* GraphQL */ `
  subscription OnCreateMessage {
    onCreateMessage {
      id
      channelID
      author
      body
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateMessage = /* GraphQL */ `
  subscription OnUpdateMessage {
    onUpdateMessage {
      id
      channelID
      author
      body
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteMessage = /* GraphQL */ `
  subscription OnDeleteMessage {
    onDeleteMessage {
      id
      channelID
      author
      body
      createdAt
      updatedAt
    }
  }
`;
