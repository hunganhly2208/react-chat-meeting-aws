// Copyright 2020-2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

import ReactDOM from 'react-dom';
import React from 'react';

import './style.css';
import App from './app';
import Amplify from '@aws-amplify/core';
import awsExports from './aws-exports';

Amplify.configure(awsExports);

window.addEventListener('load', () => {
  ReactDOM.render(<App />, document.getElementById('root'));
});
